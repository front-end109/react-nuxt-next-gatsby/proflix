import Input from "@/components/Input";
import { useCallback, useState } from "react";

const Auth = () => {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  const [variant, setVarient] = useState("login");
  const toggleVariant = useCallback(
    () =>
      setVarient((currentVariant) =>
        currentVariant === "login" ? "register" : "login"
      ),
    []
  );

  return (
    <div className="relative h-full w-full bg-[url('/images/hero.jpg')] bg-no-repeat bg-center bg-fixed bg-cover">
      <div className="bg-black w-full h-full lg:bg-opacity-60">
        <nav className="px-12 py-5">
          <img src="/images/logo.png" className="h-12" alt="Logo" />
        </nav>

        <div className="flex justify-center">
          <div className="bg-black bg-opacity-70 px-16 py-16 self-center mt-2 lg:w-2/5 lg:max-w-md rounded-md w-full">
            <h2 className="text-white text-4xl mb-8 font-semibold">
              {variant == "login" ? "Sign in" : "Register"}
            </h2>

            <div className="flex flex-col gap-4">
              {variant == "register" && (
                <Input
                  label="Username"
                  onChange={(ev: any) => {
                    setName(ev.target.value);
                  }}
                  id="name"
                  value={name}
                  type="name"
                ></Input>
              )}
              <Input
                label="Email"
                onChange={(ev: any) => {
                  setEmail(ev.target.value);
                }}
                id="email"
                value={email}
                type="email"
              ></Input>

              <Input
                label="Password "
                onChange={(ev: any) => {
                  setPassword(ev.target.value);
                }}
                id="password"
                value={password}
                type="password"
              ></Input>
            </div>

            <button className=" bg-red-600 text-white py-3 mt-10 hover:bg-red-700  px-5 w-full rounded-md transition  z-20">
              {variant == "login" ? "Login" : "Sign up"}
            </button>
            <p className="text-neutral-500 text-white mt-12">
              {variant == "login"
                ? "First time using Proflix"
                : "Already have an account?"}
              <span
                onClick={toggleVariant}
                className="text-white ml-1 hover:underline cursor-pointer font-extrabold "
              >
                {variant == "login" ? "Create an account" : "Login"}
              </span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Auth;
